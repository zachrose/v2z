# Video 2 Zeitlight

Here's what to do:

1. Install imagemagick and ffmpeg on your computer. (This can be a pain. Good luck!)
2. `cd` to this directory and run `./test` to make sure everything is set up OK.
4. Export your big video into a small video. (This will make this script less slow.)
5. Move that video file into this folder. (Let's say it's called movie.m4v)
4. Run `v2z`, piping it's output to a new zeitlight file: `./v2z movie.m4v > automated.zeitlight`
5. Wait forever for your video to be broken down into stills.
6. Wait another forever for colors to be extracted.
7. You're done! Take automated.zeitlight and run with it!
